#!/usr/bin/env node

// Test that a package created from this module can be installed and imported.
// Run `npm run pack` first. Call this script from parent directory.

const assert = require('assert');
const os = require('os');
const path = require('path');
const fs = require('fs/promises');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function main() {
    const dir = await fs.mkdtemp(path.join(os.tmpdir(), 'votepeerjs-'));
    try {
        // Copy pack into a temp dir
        console.log(dir);
        const packs = await fs.readdir("packs");
        if (packs.length === 0) {
            throw Error("No packages found. Run `npm run pack`");
        }
        const pack = packs[0];
        await fs.copyFile(
            path.join("packs", pack),
            path.join(dir, pack));

        // Initialize a new project with npm and install package.
        {
            const { stdout, stderr } = await exec(
                `npm init -y && npm install ${pack}`, {
                cwd: dir,
            });
            console.log("Output from package install:\n\n", stdout, stderr);
        }

        // Create a index.js that imports from package.
        await fs.writeFile(path.join(dir, "index.js"), `
            const assert = require('assert');
            const v = require('@bitcoinunlimited/votepeerjs');
            console.log('Dust: ', v.DEFAULT_DUST_THRESHOLD);
            assert(v.DEFAULT_DUST_THRESHOLD === 546);
        `);

        // Test that importing worked.
        {
            const { stdout, stderr } = await exec('node index.js', { cwd: dir });
            console.log("Run index.js:", stdout, stderr);
        }

    }
    finally {
        await fs.rm(dir, { recursive: true, force: true });
    }
}

main();

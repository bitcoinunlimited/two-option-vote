import { OpcodesCommon } from '@bitauth/libauth';
import InvalidVoteError from './error/invalidvoteerror';

export const OP_DATA_20 = 20;
export const OP_DATA_33 = 33;
export const OP_DATA_65 = 65;
export const OP_DATA_40 = 40;

const util = require('util');

/**
 * Last operator that acts as a direct push, meaning the value itself is pushed
 * to the stack. Values \> LAST_DATA_PUSH need to use OP_PUSHDATA_*.
 */
export const LAST_DATA_PUSH = 0x4b;

/**
 * Throws an exception unless byte at position i is a specific opcode.
 *
 * @param script - The full script
 * @param i - Position within script
 * @param wantedOpcode - The OP code we expect at position
 */
export function throwUnlessOpcode(script: Uint8Array, i: number, wantedOpcode: number) {
  if (script[i] === wantedOpcode) {
    return;
  }
  throw new InvalidVoteError(util.format(
    'Expected opcode %d at position %d, got %d',
    wantedOpcode, i, script[i],
  ));
}

/**
 * Get the value of a direct push op code, if it is one.
 */
export function getDirectPushOP(op: number): Uint8Array | null {
  switch (op) {
    case OpcodesCommon.OP_0:
      return new Uint8Array(0);
    case OpcodesCommon.OP_1NEGATE:
    case OpcodesCommon.OP_1:
    case OpcodesCommon.OP_2:
    case OpcodesCommon.OP_3:
    case OpcodesCommon.OP_4:
    case OpcodesCommon.OP_5:
    case OpcodesCommon.OP_6:
    case OpcodesCommon.OP_7:
    case OpcodesCommon.OP_8:
    case OpcodesCommon.OP_9:
    case OpcodesCommon.OP_10:
    case OpcodesCommon.OP_11:
    case OpcodesCommon.OP_12:
    case OpcodesCommon.OP_13:
    case OpcodesCommon.OP_14:
    case OpcodesCommon.OP_15:
    case OpcodesCommon.OP_16:
      /* eslint-disable-next-line no-case-declarations */
      const value = op - OpcodesCommon.OP_1 + 1;
      return Uint8Array.from(Buffer.alloc(1, value));

    default:
      return null;
  }
}

export function isPushData0(op: number): boolean {
  return (op >= 1) && (op <= LAST_DATA_PUSH);
}

/**
 * Take a copy of stack element at offset as it would appear on the stack,
 * returning the copy and the range copied from. Offset MUST point to a
 * PUSH operation
 */
export function copyStackElementAt(
  script: Uint8Array,
  offset: number,
): [Uint8Array, number, number] {
  if (offset >= script.length) {
    throw Error(`Offset '${offset}' is out of bounds. Script size is ${script.length}.`);
  }
  const op = script[offset];
  const directPush = getDirectPushOP(op);
  if (directPush !== null) {
    return [directPush, offset, offset + directPush.length];
  }
  let i = offset;
  if (isPushData0(op)) {
    ++i;
    const end = i + op - 1;
    return [script.slice(i, end + 1), i, end];
  }
  if (op === OpcodesCommon.OP_PUSHDATA_1) {
    ++i;
    const size = script[i++];
    const end = i + size - 1;
    return [script.slice(i, end + 1), i, end];
  }
  if (op === OpcodesCommon.OP_PUSHDATA_2) {
    ++i;
    const lengthLE = script.slice(i, i + 2);
    i += 2;
    /* eslint-disable-next-line no-bitwise */
    const lengthBE: number = (lengthLE[1] << 8) | lengthLE[0];
    const end = i + lengthBE - 1;
    return [script.slice(i, end + 1), i, end];
  }
  if (op === OpcodesCommon.OP_PUSHDATA_4) {
    throw Error('Not implemented');
  }
  throw Error(`Script element at offset ${offset} is no a PUSH operation (opcode: ${op})`);
}

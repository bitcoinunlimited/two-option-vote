import { MultiOptionVote } from '../multioptionvote/multioptionvote';
import { hashFromAddress } from '../transaction';
import { hash160Salted } from '../crypto';

const election = new MultiOptionVote(
  // Salt is *not* hex encoded.
  Buffer.from('4f06221a3d48da3a2965d665ee5d8b0441cfb57b65f6b7819620e391b49fc66c'),
  'Test MOV #2',
  732337,
  736657,
  ['Accept', 'Reject'],
  [
    hashFromAddress('bitcoincash:qqth3xv8z6aysxedsmwcspgkrwt7lm9whyr5fmawvc'),
    hashFromAddress('bitcoincash:qpxt75avxax8uakv47wg46rkp6ay72pnkqq5z2t325'),
    hashFromAddress('bitcoincash:qqs37r80l0enh2lykdq4cxdtz57y0amh9ss68thdye'),
  ],
);

describe('multiOptionVoteTests', () => {
  test('participantsMerkleRootHash', async () => {
    expect(Buffer.from(await election.participantsMerkleRootHash()).toString('hex'))
      .toBe('c3bd84f68c6742fb387787f2ca8993e714f3c7f09167e9497b8b473822b355dd');
  });

  test('electionID', async () => {
    expect(Buffer.from(await election.electionID()).toString('hex'))
      .toBe('c705fde5fc5bb4410f3dd6a9b78b7e4b21312638');
  });

  test('redeemScript', async () => {
    expect(Buffer.from(await election.redeemScript(election.participants[0])).toString('hex'))
      .toBe('78ad76a9141778998716ba481b2d86dd8805161b97efecaeb988527914c705fde5fc5bb4410f3dd6a9b78b7e4b21312638887b7b7e7cbb7491');
  });

  test('voterContractAddress', async () => {
    expect((await election.voterContractAddress(election.participants[0]))).toBe('bitcoincash:ppj779s6hqwagmly046ce67xmcv963wf75sxkhgv29');
  });

  test('getOptionFromHash', async () => {
    for (const o of election.voteOptions) {
      const hash = await hash160Salted(election.salt, Buffer.from(o));
      expect(await election.getOptionFromHash(hash)).toBe(o);
    }
  });
});

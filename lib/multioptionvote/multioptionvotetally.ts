import Blockchain from '../blockchain';
import NetworkTransaction from '../networktransaction';
import {
  AcceptedBallotTransaction,
  BallotResult,
} from '../tally';
import { hashToAddress } from '../transaction';
import { MultiOptionVote } from './multioptionvote';

function sortVoteTransactions(transactions: NetworkTransaction[]): NetworkTransaction[] {
  transactions.sort((a, b) => {
    // Sort transactions in mempool last.
    const aHeight = a.height <= NetworkTransaction.HEIGHT_UNCONFIRMED
      ? 0xEEF00D
      : a.height;
    const bHeight = a.height <= NetworkTransaction.HEIGHT_UNCONFIRMED
      ? 0xEEF00D
      : b.height;

    if (aHeight === bHeight) {
      // Heights are the same. Order by transaction ID.
      return Buffer.from(a.txIdBE).compare(b.txIdBE);
    }
    return aHeight < bHeight ? -1 : 1;
  });
  return transactions;
}

async function fetchVote(
  electrum: Blockchain,
  election: MultiOptionVote,
  voteContract: string,
): Promise<[NetworkTransaction, Uint8Array, Uint8Array] | undefined> {
  const txs = sortVoteTransactions(
    await electrum.getSpendingTxs(voteContract, election.beginHeight, election.endHeight, true),
  );
  const voteOptionsHashed = await election.voteOptionsHashed;
  for (const tx of txs) {
    try {
      const [vote, signature] = MultiOptionVote.parseVote(tx.getTransaction(), voteOptionsHashed);
      return [tx, vote, signature];
    } catch (e) {
      // It's an invalid vote on parse error.
      console.log('Invalid vote', e);
      continue;
    }
  }
  return undefined;
}

export async function fetchMultiOptionVotes(
  electrum: Blockchain,
  election: MultiOptionVote,
): Promise<BallotResult> {
  const voteContractAddresses = election.participants
    .map(async (p): Promise<[Uint8Array, string]> => [p, await election.voterContractAddress(p)]);

  const votes = await Promise.all(voteContractAddresses
    .map(async (x)
    : Promise<[Uint8Array, [NetworkTransaction, Uint8Array, Uint8Array] | undefined]> => {
      const [participant, contractAddress] = await x;
      return [participant, await fetchVote(electrum, election, contractAddress)];
    }));

  const accepted: AcceptedBallotTransaction[] = [];

  for (const [participant, parsedVoteTx] of votes) {
    if (parsedVoteTx === undefined) {
      continue;
    }
    const [transaction, vote, signature] = parsedVoteTx;
    accepted.push({
      transaction,
      vote,
      signature,
      participant: hashToAddress(participant, 'p2pkh'),
    });
  }

  return {
    accepted,
    rejected: [],
  };
}

import { OP_DATA_20 } from '../script';
import {
  hash160, signHashOfMessageWithSchnorr,
} from '../crypto';
import { TwoOptionVote } from './twooptionvote';
import { hashToAddress } from '../transaction';
import { numberToUint32BE } from '../utilserialize';

const assert = require('assert');

/**
 * The proposal ID consist of a hash of all the data belonging to an election.
 *
 * @param election - Election data.
 */
export async function calculateProposalID(election: TwoOptionVote): Promise<Uint8Array> {
  if (!(election.endHeight >= 0)) {
    throw Error('Invalid endHeight');
  }

  // use concat to take a copy
  const participants = election.votersPKH.concat().sort(Buffer.compare);

  const proposalIDParts: Buffer[] = [
    Buffer.from(election.salt),
    Buffer.from(election.description),
    Buffer.from(election.optionA),
    Buffer.from(election.optionB),
    numberToUint32BE(election.endHeight),
    Buffer.concat(participants),
  ];

  const blob = Buffer.concat(proposalIDParts);
  return hash160(blob);
}

/**
 * Get the redeemscript of a two-option-vote contract.
 *
 * A transaction unlocking this script casts a vote.
 *
 * @param proposalID - Proposal ID
 * @param optionA - Vote option
 * @param optionB - Vote option
 * @param voterPKH - The voter who may solve this script.
 */
export function twoOptionContractRedeemscript(
  proposalID: Uint8Array, optionA: Uint8Array, optionB: Uint8Array, voterPKH: Uint8Array,
): Buffer {
  const contractCode = '5479a988547a5479ad557a5579557abb537901147f75537a887b01147f77767b8778537a879b7c14beefffffffffffffffffffffffffffffffffffff879b';
  const redeemScript = Buffer.from(contractCode, 'hex');

  return Buffer.concat([
    Buffer.alloc(1, OP_DATA_20), proposalID,
    Buffer.alloc(1, OP_DATA_20), optionB,
    Buffer.alloc(1, OP_DATA_20), optionA,
    Buffer.alloc(1, OP_DATA_20), voterPKH,
    redeemScript]);
}

/**
 * Find the two-option-vote contract bitcoin cash address for specified voter.
 *
 * @param proposalID - Proposal ID
 * @param optionA - A vote option
 * @param optionB - A vote option
 * @param voterPKH - The voter the address belongs to.
 */
export async function deriveContractAddress(
  proposalID: Uint8Array, optionA: Uint8Array, optionB: Uint8Array, voterPKH: Uint8Array,
): Promise<string> {
  const hash = await hash160(twoOptionContractRedeemscript(
    proposalID, optionA, optionB, voterPKH,
  ));

  return hashToAddress(hash, 'p2sh');
}

/**
 * Vote message consists of proposal ID and a vote option concatenated.
 */
export function createVoteMessage(proposalID: Uint8Array, option: Uint8Array): Uint8Array {
  assert(proposalID.length === 20);
  assert(option.length === 20);
  return Buffer.concat([proposalID, option]);
}

/**
 * Signs a vote message with given private key. Vote message is proposal ID
 * and a vote option concatenated.
 */
export async function signVoteMessage(
  privateKey: Uint8Array, message: Uint8Array,
): Promise<Uint8Array> {
  assert(message.length === 40);
  return signHashOfMessageWithSchnorr(privateKey, message);
}

/**
* As defined in policy/policy.cpp in Bitcoin Unlimited
*/
export const MAX_TX_IN_SCRIPT_SIG_SIZE = 1650;

/**
 * Size of a Schnorr signature in a Bitcoin Cash transaction.
 */
export const SCHNORR_SIGNATURE_SIZE = 65;

/**
 * Size of a public key (compact representation)
 */
export const PUBLIC_KEY_COMPACT_SIZE = 33;

/**
 * The minimum output amount (as defined in policy/policy.h in Bitcoin Unlimited)
 */
export const DEFAULT_DUST_THRESHOLD = 546;

/**
 * Size of a transaction input with an empty script.
 */
export const EMPTY_TX_INPUT_SIZE = (
  32 /* txid */
        + 4 /* vout */
        + 2 /* scriptsig size varint */
        + 0 /* scriptsig */
        + 4 /* sequence */
);

/**
         * Size of a P2PKH input signed using Schnorr
         */
export const P2PKH_SCHNORR_INPUT_SIZE = (
  EMPTY_TX_INPUT_SIZE
                + 1 /* extra byte for the scriptsig size varint */
            + 99 /* scriptsig: push <signature 64 bytes> push <pubkey 33 bytes> */
);

/**
         * Size of a P2PKH output
         */
export const P2PKH_OUTPUT_SIZE = (
  8 /* value */
            + 2 /* scriptPubKey size */
            + 25 /* ScriptPubKey: OP_DUP OP_HASH160 push <hash> OP_EQUALVERIFY OP_CHECKSIG */
);

/**
         * Size of a P2SH output
         */
export const P2SH_OUTPUT_SIZE = (
  8 /* value */
            + 2 /* scriptPubKey size */
            + 23 /* ScriptPubKey: OP_HASH160 push <hash> OP_EQUAL */
);
